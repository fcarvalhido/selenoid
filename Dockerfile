FROM golang:1.17 as builder
RUN go get github.com/aws/aws-sdk-go/aws
RUN git clone https://github.com/aerokube/selenoid
WORKDIR /go/selenoid/
RUN echo $(go version)
RUN echo $(go mod tidy)
RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -tags 's3' .

FROM aerokube/selenoid
RUN apk --no-cache add bash docker
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/selenoid/selenoid /usr/bin/selenoid
COPY browsers.json /etc/selenoid/browsers.json
COPY logging.json /etc/selenoid/logging.json
COPY entrypoint.sh /entrypoint.sh
RUN chmod 777 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
