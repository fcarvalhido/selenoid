#!/bin/bash
exec /usr/bin/selenoid \
  -listen :4444 \
  -limit 4 \
  -conf /etc/selenoid/browsers.json \
  -log-conf /etc/selenoid/logging.json \
  -video-output-dir /opt/selenoid/video \
  -log-output-dir /opt/selenoid/log \
  -service-startup-timeout 30s \
  -session-attempt-timeout 30s \
  -session-delete-timeout 20s \
  -session-attempt-timeout 20s \
  -timeout 5m

# -s3-endpoint https://s3.eu-west-3.amazonaws.com \
# -s3-region $AWS_REGION \
# -s3-bucket-name $AWS_BUCKET_NAME \